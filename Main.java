import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {


    public static void main (String[] args) throws IOException {

        NameGenerator generator = new NameGenerator();
        generator.menu ();
        generator.run ();



    }

}

