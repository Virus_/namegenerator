import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class  NameBase {
    private static final int ONE = 1, TWO = 2;
    static boolean manOrWoman;
    ArrayList<String> readFile (ArrayList<String> boyName, ArrayList<String> girlName, int number) throws IOException {

        ArrayList<String> nameList = new ArrayList<> (  );

        switch (number) {
            case ONE -> manOrWoman = true;
            case TWO -> manOrWoman = false;
        }

        String path = manOrWoman ? "/home/sebastian/Pulpit/nameGenerator/boyName.txt" : "/home/sebastian/Pulpit/nameGenerator/girlName.txt";

        BufferedReader reader = new BufferedReader (
                new FileReader (path));
        try {
            String text;
            while ((text = reader.readLine ( )) != null) {

                if (manOrWoman) {
                    boyName.add (text);
                } else
                    girlName.add (text);
            }

            reader.close ( );

        } catch (IOException e) {

            System.out.println (e);
        }

        return manOrWoman ? boyName : girlName;

    }






}



